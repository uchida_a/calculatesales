package jp.alhinc.uchida_aimi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//変数用クラス
class Variable {
	String branchKey;
	String branchName;
	Long sales = 0L;

	public Variable(String key, String name) {
		this.branchKey = key;
		this.branchName = name;
	}
}

public class CalculateSales {

	public static void main(String[] args) {

		// 支店定義ファイル読込

		Map<String, Variable> branchmap = new HashMap<>(); //Mapインターフェース#HashMapクラスをMap型<キー,値>で宣言

		try {
			File branchFile = new File(args[0], "branch.lst");
			if (branchFile.exists() == false) { //File#exist･･･指定ファイルが存在するか確認(boolean型を返す)
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(branchFile)); //branchFileを読み込む

				String line;
				while ((line = br.readLine()) != null) { //br#readLineで1行読んで変数lineに格納⇒nullじゃなかったらループ
					String[] branch = line.split(","); //文字列型の配列branchに1行読んでカンマで区切った文字列を格納

					//配列の中身が2つでない or 配列branchの0番目が"[0-9]{3}"ではない or 配列branchの1番目に何にも文字が入ってないとき
					if (branch.length != 2 || branch[0].matches("[0-9]{3}") == false || branch[1].matches(".+") == false) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}

					//branchmapに格納(キー：branch[0](支店コード)、値：Varibleにbranch[0]と[1](支店名)を渡す＆Mapに入れる)
					branchmap.put(branch[0], new Variable(branch[0], branch[1]));
				}
			} finally {
				br.close();
			}

			// 売上ファイル取出

			BufferedReader salesbr = null;

			//Fileクラスはファイルを扱いたいときに使う
			//File file = new File(扱いたいファイル名[ディレクトリの位置＋ファイル名]);
			//File#listFiles･･･指定したディレクトリに含まれるファイルやディレクトリを一覧として取得→配列として返す

			//FilenameFilterでフィルタリングしたファイルをlistFilesに渡して、File型の配列 list に入れる
			File[] list = new File(args[0]).listFiles(new FilenameFilter() {

				//File#FilenameFilter･･･指定した条件のファイルのみをフィルタリングするクラス
				//accept･･･FilenameFilterのコンストラクタ。指定されたファイルをリストに含めるか判定する。
				@Override
				public boolean accept(File dir, String name) {
					//File#isFile･･･指定されたディレクトリ&ファイル名が「ファイルなのかどうか」を調べる
					//指定されたものがファイル かつ ファイル名が"[0-9]{8}.rcd"の形式のものを返す
					return new File(dir, name).isFile() && name.matches("[0-9]{8}.rcd");
				}
			});

			Arrays.sort(list);

			int n = 0; //変数nに0を入れる(基準の数値として使用)

			//拡張for文･･･配列やコレクションの要素を一つずつ順番に取り出して、その各々の要素に対して処理を実行することができる。
			//listの要素を1つずつ取り出して File型の変数 salesFile に入れて処理を行う
			for (File salesFile : list) {
				//File#getName･･･ファイル名(sample.txtみたいな感じ)を取得
				String filename = salesFile.getName();

				// 例外処理
				//File#substring･･･売上ファイル名「00000000.rcd」の0文字目～8文字目を切り出す
				String str1 = filename.substring(0, 8);
				int num = Integer.parseInt(str1); //条件式で扱うためにint型に変換

				//nが0でない(初期代入値でない) かつ ファイル名の数字部分とn(基準の数値)の差が1でない
				if (n != 0 && (num - n) != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;  //if文を離脱
				}

				// ファイル読込
				try {

					//FileReaderクラス･･･「このファイルを読み込むよ～」の指示。(これにファイル名を渡す)
					//↑をまるごとBufferedReaderクラス(ファイルをまとめて読み込むクラス)に渡す
					salesbr = new BufferedReader(new FileReader(salesFile));

					//売上ファイルを一行読む(1行目＝支店コード)→変数codeに入れる
					//同じくもう一行読む(2行目＝売上金額)→変数amountに入れる
					String code = salesbr.readLine();
					String amount = salesbr.readLine();

					// 例外処理
					//Map#containsKey･･･指定したキーが存在するか確認→キーが存在する場合はtrueを返す
					//branchmapに 読み込んだ支店コード と一致するものがなかったら
					if (branchmap.containsKey(code) == false) {
						System.out.println(filename + "の支店コードが不正です");
						return;
					}

					//2行以上あったら
					if (salesbr.readLine() != null) {
						System.out.println(filename + "のフォーマットが不正です");
						return;
					}

					//売上金額をLong型にして salesresultに入れる
					Long salesresult = Long.parseLong(amount);

					//Variableクラスを使うよ～の宣言
					//ranchmapから、読み込んだ支店コードに対応する支店名を取得→変数vに入れる
					Variable v = branchmap.get(code);

					//合計金額加算･･･Variableクラスのsalesに、既に入ってた金額と今読み込んだ金額を足したものを入れる
					v.sales = v.sales + salesresult;
					if (v.sales.toString().length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;

					}
					branchmap.put(code, v);
					n = num;
				} finally {
					salesbr.close();
				}

				// 支店別集計ファイル作成
				// 全支店コード、支店名、合計金額出力
				PrintWriter pw = null;
				try {
					pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(args[0], "branch.out"))));

					for (Variable v : branchmap.values()) {
						pw.println(v.branchKey + "," + v.branchName + "," + v.sales);
					}

				} finally {
					pw.close();
				}
			}//for文終了
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}
	}
}
